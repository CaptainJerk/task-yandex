/******************************************************************************
 * File:        mempool.c
 * Author:      captain 
 * Date:        21.10.2019
 *              
 *****************************************************************************/

#include "mempool.h"

#define __HDR_MSK (1 << (8 * sizeof(__mp_hdr_t) - 1))

static void *strip_header(uintptr_t block_ptr)
{
    return (void *) ((max_align_t *) block_ptr + 1);
}

static bool block_is_allocated(__mp_hdr_t h)
{
    return (h & __HDR_MSK);
}

static __mp_hdr_t block_head(__mp_hdr_t h)
{
    return (h & ~__HDR_MSK);
}

static void mark_block_allocated(__mp_hdr_t *h)
{
    *h = block_head(*h) | __HDR_MSK;
}

static void memp_ctor(  memp_t *mp, 
                        uint32_t block_size, 
                        uint32_t block_count )
{
    mp->block_size      = block_size;
    mp->block_count     = block_count;
    mp->head            = 0;
    mp->step            = __BLOCK_SIZE(block_size);
}

static void construct_data_layout(memp_t *mp)
{
    /* Pool layout:
     * +------+---------+---------++------+----
     * | head | padding | payload || head | ...
     * +------+---------+---------++------+----
     * As the data pointer is implicitly aligned (malloc or 
     * through static declaration), I only need to enforce
     * an alignment requirement for a block payload.
     * This achieved by padding the head pointer up to
     * the alignment size.
     * Each blocks head points to the next free block in the pool.
     * Upper bit of the head pointer is set to 1 when the 
     * block is allocated, so that the memp_free can perform pointer
     * checks.
     */
    for (uint32_t i = 0; i < mp->block_count; i++) {
        *(__mp_hdr_t *) (mp->data + i * mp->step) = i + 1;
    }
}

memp_t memp_create( uint32_t block_size,
                    uint32_t block_count,
                    void *data )
{
    memp_t ret;
    memp_ctor(&ret, block_size, block_count);
    ret.data = (uintptr_t) data;
    /* NULL check does not yield any report! */
    if (data) construct_data_layout(&ret);

    return ret;
}

void *memp_alloc(memp_t *mp)
{
    /* Free space check */
    if (!mp || !mp->data || mp->head >= mp->block_count) return NULL;

    /* Calculate an address of the next unallocated block. */
    uintptr_t block = mp->data + mp->head * mp->step;
    /* The header here points to the next unallocated block.
     * Assign it to the next head pointer. */
    mp->head = block_head(*(__mp_hdr_t *) block);
    /* Mark block as allocated */
    mark_block_allocated((__mp_hdr_t *) block);

    /* Strip the header/padding off of the payload and return */
    return strip_header(block);
}

void *memp_free(memp_t *mp, void *ptr)
{
    uintptr_t _ptr = (uintptr_t) ptr;

    /* Sanity checks */
    if (!mp || !mp->data || !ptr || _ptr < mp->data) return NULL;

    /* Calculate block number and check if it is located within the pool */
    __mp_hdr_t num = (_ptr - mp->data) / mp->step;
    if (num >= mp->block_count) return NULL;

    /* Pointer to the block being deallocated. This will now point
     * to the one queued for allocation on the next memp_alloc call. */
    uintptr_t block_ptr = mp->data + num * mp->step;

    /* Do not want to deallocate a free block */
    if (!block_is_allocated(*(__mp_hdr_t *) block_ptr)) return NULL;

    *(__mp_hdr_t *) block_ptr = mp->head;
    /* Update the running head value */
    mp->head = num;
    /* Return payload pointer */
    return strip_header(block_ptr);
}

/******************************** End of file ********************************/
