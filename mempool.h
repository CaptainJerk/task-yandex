/******************************************************************************
 * File:        mempool.h
 * Author:      captain 
 * Date:        21.10.2019
 * Description: Memory pool with O(1) allocation complexity.
 *              The implementation is intrinsically reentrant as it does not
 *              rely on global variables. Yet it is not exactly thread-safe,
 *              to comply with the task requirements, I've implemented
 *              a wrapper module (see ts_mempool.h for more details).
 *              
 *****************************************************************************/

#ifndef _MEMPOOL_H_
#define _MEMPOOL_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>

#define __ALIGN_SIZE        (sizeof(max_align_t))
#define __BLOCK_SIZE(bs)    ((bs / __ALIGN_SIZE + 2) * __ALIGN_SIZE)
#define POOL_DATA_SIZE(bs, bc) \
                            (__BLOCK_SIZE(bs) * bc)
#define POOL_DATA_STATIC_ALLOCATOR(name, bs, bc) \
    max_align_t name[POOL_DATA_SIZE(bs, bc) / __ALIGN_SIZE]

#ifdef __cplusplus
extern "C" {
#endif

typedef uint32_t __mp_hdr_t;
typedef struct {
    __mp_hdr_t  head;
    uint32_t    block_count;
    uint32_t    block_size;
    uint32_t    step;
    uintptr_t   data;
} memp_t;

/**
 * Create and initialize a new static memory pool.
 * @param block_size Block size in bytes
 * @param block_count Number of blocks
 * @param data Memory pool storage. Can be allocated in heap:
 *              data = malloc(POOL_DATA_SIZE(block_size, block_count));
 *              or in static memory:
 *              POOL_DATA_STATIC_ALLOCATOR(data, block_size, block_count);
 *              !NB! This call does not perform the pointer check - if not
 *              done explicitly right away, it will be deferred until the
 *              next memp_alloc call.
 *
 * @return mempool_t instance.
 */
memp_t memp_create(uint32_t block_size, uint32_t block_count, void *data);

/**
 * Allocate a block.
 * @param mp mempool_t instance pointer
 * @return Pointer to the allocated block or NULL if failed.
 */
void *memp_alloc(memp_t *mp);

/**
 * Deallocate.
 * @param mp mempool_t instance pointer
 * @param Pointer to an address within the block.
 * @return For testing purpose, the function returns
 *          starting pointer of the deallocate dmemory block.
 */
void *memp_free(memp_t *mp, void *ptr);

#ifdef __cplusplus
}
#endif

#endif /* _MEMPOOL_H_ */

/******************************** End of file ********************************/
