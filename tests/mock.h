/******************************************************************************
 * File:        mock.h
 * Author:      captain 
 * Date:        07.02.2020
 * Description: Kill the mockingbird :)
 *              
 *****************************************************************************/

#ifndef _MOCK_H_
#define _MOCK_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define test_assert(cond, ...)  \
    while(!(cond)) {            \
        printf(__VA_ARGS__);    \
        exit(-1);               \
    }

#ifdef __cplusplus
extern "C" {
#endif

/* Objects to mimick block data.
 * For each test I will instantiate 3 memory pools with
 * block size being (wrt largest data type):
 * 0 - block_s_t - less
 * 1 - block_m_t - equal 
 * 2 - block_x_t - greater
 */
typedef uint8_t mock_s_t[4];
typedef uint8_t mock_m_t[sizeof(max_align_t)];
typedef uint8_t mock_x_t[sizeof(max_align_t) + 3];

/**
 * Get mock object name
 * @param id mock object type id
 * @return M/S/X
 */
const char *mock_name(int id);

/**
 * Get mock object size
 * @param id mock object type id
 * @return size in bytes
 */
const int mock_size_by_id(int id);

/**
 * Check if dummy is aligned.
 * @param obj
 * @return true if aligned
 */
bool is_aligned(void *obj);

/**
 * Get mock object size
 * @param obj
 * @return size in bytes
 */
int mock_object_size(void *obj);

#ifdef __cplusplus
}
#endif

#endif /* _MOCK_H_ */

/******************************** End of file ********************************/

