/******************************************************************************
 * File:        test_0.c
 * Author:      captain 
 * Date:        07.02.2020
 *              Single block memory pool tests
 *****************************************************************************/

#include "mempool.h"
#include "mock.h"

#define __SZ(n) (n * sizeof(max_align_t))

/* This should yield exactly 2 chunks of max_align_t size */
POOL_DATA_STATIC_ALLOCATOR(s, sizeof(mock_s_t), 1);
/* 3 chunks */
POOL_DATA_STATIC_ALLOCATOR(m, sizeof(mock_m_t), 1);
POOL_DATA_STATIC_ALLOCATOR(x, sizeof(mock_x_t), 1);

int in_vec[3] = {sizeof(s), sizeof(m), sizeof(x)};
int cmp_vec[3] = {__SZ(2), __SZ(3), __SZ(3)};
memp_t mp[3];

int main(void)
{
    /* Allocated size test */
    for (int i = 0; i < 3; i++) {
        test_assert(in_vec[i] == cmp_vec[i], 
                    "%s has unexpected size: %i, " \
                    "expected: %i\n", mock_name(i), in_vec[i], cmp_vec[i]);
    }

    /* Initialize */
    mp[0] = memp_create(mock_size_by_id(0), 1, s);
    mp[1] = memp_create(mock_size_by_id(1), 1, m);
    mp[2] = memp_create(mock_size_by_id(2), 1, x);

    /* The pool should only hold one element. An attempt to allocate more
     * yields test fail */
    for (int i = 0; i < 3; i++) {
        void *a = memp_alloc(&mp[i]);
        void *b = memp_alloc(&mp[i]);
        test_assert(NULL != a && NULL == b,
                    "Pool %s over allocation (%p, %p, %d, %d)\n", 
                    mock_name(i), a, b, mp[i].head, mp[i].block_count);
        /* Deallocate once */
        a = memp_free(&mp[i], a);
        b = memp_free(&mp[i], b);
        test_assert(NULL != a && NULL == b,
                    "Pool %s over deallocation (%p, %p, %d, %d)\n",
                    mock_name(i), a, b, mp[i].head, mp[i].block_count);
    }

    return 0;
}

/******************************** End of file ********************************/

