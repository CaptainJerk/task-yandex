/******************************************************************************
 * File:        test_1.c
 * Author:      captain 
 * Date:        07.02.2020
 *              Multiple blocks allocation tests
 *****************************************************************************/

#include "mock.h"
#include "mempool.h"

#define __SIZE (128)

POOL_DATA_STATIC_ALLOCATOR(s, sizeof(mock_s_t), __SIZE);
POOL_DATA_STATIC_ALLOCATOR(m, sizeof(mock_m_t), __SIZE);
POOL_DATA_STATIC_ALLOCATOR(x, sizeof(mock_x_t), __SIZE);

void *objects[3][__SIZE];
int allocated_headers[__SIZE];
memp_t mp[3];

int main(void)
{
    mp[0] = memp_create(mock_size_by_id(0), __SIZE, s);
    mp[1] = memp_create(mock_size_by_id(1), __SIZE, m);
    mp[2] = memp_create(mock_size_by_id(2), __SIZE, x);

    /* Prefill pools */
    for (int j = 0; j < 3; j++) {
        for (int i = 0; i < __SIZE; i++) {
            /* Will use single mp.head map for all mocks and
             * clear it on each pass. Because why not.. */
            allocated_headers[i] = -1;
        }
        for (int i = 0; i < __SIZE; i++) {
            test_assert(mp[j].head < __SIZE, 
                        "Pool %s head pointer is out of bounds: %i\n",
                        mock_name(j), mp[j].head);
            test_assert(allocated_headers[mp[j].head] < 0,
                        "Pool %s has an overlapping allocation in block %i\n",
                        mock_name(j), mp[j].head);
            /* Add new header to the map */
            allocated_headers[mp[j].head] = 1;
            void *obj = memp_alloc(&mp[j]);
            /* Oops... premature pool depletion */
            test_assert(NULL != obj, 
                        "Pool %s has unexpectedly ran out of blocks at %i\n",
                        mock_name(j), i);
            /* Alignment check */
            test_assert(is_aligned(obj), 
                        "Pool %s has allocated an unaligned block %p (%i)\n",
                        mock_name(j), obj, i);
            /* Save for the future */
            objects[j][i] = obj;
        }
    }

    /* One more allocation should fail */
    for (int i = 0; i < 3; i++) {
        void *ret = memp_alloc(&mp[i]);
        test_assert(NULL == ret, "Pool %s overallocation\n", mock_name(i));
    }

    /* Deallocation test */
    for (int j = 0; j < 3; j++) {
        for (int i = 0; i < __SIZE; i++) {
            void *ret = memp_free(&mp[j], objects[j][i]);
            test_assert(ret == objects[j][i], 
                        "Pool %s deallocation error: (%p, %p)\n",
                        mock_name(j), ret, objects[j][i]);
        }
    }

    /* Sainity checks test for coverage */
    for (int j = 0; j < 3; j++) {
        memp_alloc(&mp[j]);
        void *ret = memp_alloc(&mp[j]);
        memp_alloc(&mp[j]);
        /* Attempt to deallocate a free block twice */
        memp_free(&mp[j], ret);
        ret = memp_free(&mp[j], ret);
        test_assert(NULL == ret, "Pool %s allows to deallocate free blocks %p\n",
                    mock_name(j), ret);
        /* No valid pool data had been assigned during initialization */
        mp[j].data = (uintptr_t) NULL;
        ret = memp_alloc(&mp[j]);
        test_assert(NULL == ret, "Pool %s allocated an object from NULL %p\n",
                    mock_name(j), ret);
        ret = memp_free(&mp[j], NULL);
        test_assert(NULL == ret, "Pool %s NULL deallocation returned %p\n",
                    mock_name(j), ret);
    }

    return 0;
}

/******************************** End of file ********************************/

