/******************************************************************************
 * File:        test_2.c
 * Author:      captain 
 * Date:        08.02.2020
 *              Thread-safe wrapper test.
 *****************************************************************************/

#include "mock.h"
#include "ts_mempool.h"

#define __SIZE (128)

POOL_DATA_STATIC_ALLOCATOR(x1, sizeof(mock_x_t), __SIZE);
POOL_DATA_STATIC_ALLOCATOR(x2, sizeof(mock_x_t), __SIZE);

bool lock_fail = true, unlock_fail = true;

static void lock(void) { 
    lock_fail = false;
    printf("Object is locked\n");
}

static void unlock(void) {
    unlock_fail = false;
    printf("Object is unlocked\n");
}

int main(void)
{
    ts_memp_t mp1 = ts_memp_create(mock_size_by_id(3), __SIZE, x1, lock, unlock);
    ts_memp_t mp2 = ts_memp_create(mock_size_by_id(3), __SIZE, x2, NULL, NULL);

    void *ret1 = ts_memp_alloc(&mp1);
    /* This should not cause segfaults */
    void *ret2 = ts_memp_alloc(&mp2);
    test_assert(NULL != ret1 && NULL != ret2, "Allocation failed\n");

    return lock_fail || unlock_fail;
}

/******************************** End of file ********************************/

