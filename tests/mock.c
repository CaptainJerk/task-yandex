/******************************************************************************
 * File:        mock.c
 * Author:      captain 
 * Date:        07.02.2020
 *              
 *****************************************************************************/

#include "mock.h"

#define __ID(n) (n % 3)

static const char *names[] = {"S", "M", "X"};
static const int sizes[] = {sizeof(mock_s_t), sizeof(mock_m_t), sizeof(mock_x_t)};

const char *mock_name(int id)
{
    return names[__ID(id)];
}

const int mock_size_by_id(int id)
{
    return sizes[__ID(id)];
}

int mock_object_size(void *obj)
{
    return (int) *((uint8_t *) obj + 1);
}

bool is_aligned(void *obj)
{
    uintptr_t ptr = (uintptr_t) obj;
    return 0 == (ptr % sizeof(max_align_t));
}

/******************************** End of file ********************************/

