/******************************************************************************
 * File:        ts_mempool.h
 * Author:      captain 
 * Date:        07.02.2020
 * Description: Thread-safe memory pool wrapper.
 *              The module extends mempool, by adding two virtual methods, which
 *              are scheduler/RTOS dependent:
 *              1. lock - lock a mutex or enter critical zone;
 *              2. unlock - unlock mutex or restores the scheduler state;
 *
 *              !NB! if these calls are not reentrant - all 
 *              allocations/deallocations from within already locked state
 *              should be performed through mempool calls.
 *
 *****************************************************************************/

#ifndef _TS_MEMPOOL_H_
#define _TS_MEMPOOL_H_

#include "mempool.h"
#define __CALL(fn) if (NULL != fn) { fn(); }

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    memp_t mp;
    void (*lock)(void);
    void (*unlock)(void);
} ts_memp_t;

/**
 * Create and initialize a new static memory pool.
 * @param block_size Block size in bytes
 * @param block_count Number of blocks
 * @param data Memory pool storage
 * @param lock Object lock function. A thread that is trying to
 *              lock already locked pool, should block or yield
 *              immediately.
 * @param unlock Unlock function
 * @return ts_memp_t instance.
 */
static inline ts_memp_t ts_memp_create( uint32_t block_size, 
                                        uint32_t block_count, 
                                        void *data,
                                        void (*lock)(void),
                                        void (*unlock)(void) )
{
    ts_memp_t ret;
    ret.mp = memp_create(block_size, block_count, data);
    ret.lock = lock;
    ret.unlock = unlock;
    return ret;
}

/**
 * Allocate a block.
 * @param mp ts_mempool_t instance pointer
 * @return Pointer to the allocated block or NULL if failed.
 */
static inline void *ts_memp_alloc(ts_memp_t *mp)
{
    __CALL(mp->lock);
    void *ret = memp_alloc(&mp->mp);
    __CALL(mp->unlock);
    return ret;
}

/**
 * Deallocate.
 * @param mp ts_mempool_t instance pointer
 * @param Pointer to an address within the block.
 */
static inline void *ts_memp_free(ts_memp_t *mp, void *ptr)
{
    __CALL(mp->lock);
    void *ret = memp_free(&mp->mp, ptr);
    __CALL(mp->unlock);
    return ret;
}

#ifdef __cplusplus
}
#endif

#endif /* _TS_MEMPOOL_H_ */

/******************************** End of file ********************************/

